def fizzBuzz(ra):
    for num in range(1, ra + 1):
        if num % 15 == 0:
            print("FizzBuzz", end=" ")
            continue
        elif num % 3 == 0:
            print("Fizz", end=" ")
            continue
        elif num % 5 == 0:
            print("Buzz", end=" ")
            continue
        else:
            print(num, end=" ")
            continue


r = int(input("Enter range: "))
fizzBuzz(r)
