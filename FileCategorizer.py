import os
import shutil as sh
from pathlib import Path
import time
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from dotenv import load_dotenv

load_dotenv()

MAIN_DIRECTORY_PATH = os.getenv("FC_MAIN_DIRECTORY_PATH")

# Contains all methods for Handling IO Ops
class FileCategorizerCustomClass:
    def GetFileList(self, path):
        directoryList = os.listdir(path)
        return directoryList

    def choosePathForFile(self, fileName):
        [name, extension] = fileName.rsplit(".")

        if "PSR" in name:
            return "PSR"
        elif extension == "png" or extension == "jpeg":
            return "Images"
        elif extension == "pdf":
            return "PDFs"
        elif extension == "exe":
            return "executables"
        elif extension == "docx" or extension == "doc":
            if "#" in name or "trello" in name.toLower():
                return "Trello_docs"
            return "docs"
        else:
            return ""

    def moveFiletoPath(self, name, distPath):
        src = os.path.join(MAIN_DIRECTORY_PATH, name)
        distLoc = os.path.join(distPath, name)
        isExists = os.path.isdir(distPath)

        print(isExists)
        if not isExists:
            os.mkdir(distPath)

        sh.move(src, distLoc)

    def dealWithThis(self, path):
        try:
            fileName = path.replace(MAIN_DIRECTORY_PATH, "")
            distPath = os.path.join(
                MAIN_DIRECTORY_PATH, self.choosePathForFile(fileName)
            )
            self.moveFiletoPath(fileName, distPath)
        except Exception as e:
            print(e)


class Watcher:
    def __init__(self):
        # Initializing the observer
        self.observer = Observer()

    def run(self):
        event_handler = EventHandler()
        self.observer.schedule(event_handler, MAIN_DIRECTORY_PATH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except Exception as e:
            self.observer.stop()
            print("Observer Stopped")
            print(e)
        self.observer.join()


class EventHandler(LoggingEventHandler):
    def __init__(self):
        self.fIO = FileCategorizerCustomClass()

    def dispatch(self, event):
        if not event.is_directory and event.event_type == "created":
            print("New File created" + event.src_path)
            self.fIO.dealWithThis(event.src_path)


if __name__ == "__main__":
    watch = Watcher()
    watch.run()

# fileIO = FileCategorizerCustomClass()
# print(fileIO.GetFileList(MAIN_DIRECTORY_PATH))

# print(fileIO.choosePathForFile("#60_-_feedback_points_23-05-2022.docx"))
# # choosePathForFile("#60_-_feedback_points_23-05-2022.docx")
